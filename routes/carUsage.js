const express = require('express');
const router = express.Router();
const db = require('../config/database');
const CarUsage = require('../models/CarUsage');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var jwt = require('jsonwebtoken');

router.get('/allCarUsage', verifyToken,(req, res) => {

    jwt.verify(req.token, 'key', (err, data) => {
        if(err){
            res.sendStatus(403);
        }else{
            CarUsage.findAll()
            .then(result => {
                res.send(result);
            })
            .catch(err => {
                console.log(err);
                res.send('Error');
            });
        }
    });

    // CarUsage.findAll()
    // .then(result => {
    //     res.send(result);
    // })
    // .catch(err => {
    //     console.log(err);
    //     res.send('Error');
    // });
});

function verifyToken(req, res, next){
    const header = req.headers['authorization'];
    if(typeof header !== 'undefined'){
        const bearer = header.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403);
    }
}

router.post('/createCarUsage', jsonParser , function(req, res, next){
    var jsondata = req.body;
    const data = CarUsage.build(jsondata);
    data.save()
    .then(function(result){
        res.sendStatus(200);
    }).catch(function(err){
        console.log(err);
        res.send('Error');
    });
});

router.delete('/deleteCarUsage', function(req, res, next){
    CarUsage.findAll({
        where: {
          car_id: req.query.carId
        }
    })
    .then(function(result){
        if(result.length != 0){
            for(var i = 0; i < result.length ; i++){
                CarUsage.destroy(
                    { where: { id: result[i].id } 
                })
                .then(function(){
                    res.sendStatus(200);
                })
                .catch(function(err){
                    console.log(err);
                    res.send('Error');
                });
            }
        }else{
            res.send("This car id is not available");
        }
    })
    .catch(function(err){
        console.log(err);
        res.send('Error');
    });
});

router.post('/updateCarUsage', jsonParser , function(req, res, next){
    var jsondata = req.body;
    CarUsage.findOne(
        { 
            where: { car_id: jsondata.car_id } 
        }
    )
    .then(function(result){
        if(result != null){
            if(jsondata.id === result.id){
                CarUsage.update(
                    jsondata,
                    { where: { id: jsondata.id } }
                )
                .then(function(result1){
                    res.sendStatus(200);
                })
                .catch(function(err){
                    console.log(err);
                    res.send('error');
                });
            }else{
                res.send("car id is not matched");
            }
        }else{
            res.send("car id is not available");
        }
    })
    .catch(function(err){
        console.log(err);
        res.send('error');
    });
});

module.exports = router;