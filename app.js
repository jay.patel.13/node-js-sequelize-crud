const express = require('express');
var app = express();
const db = require('./config/database');
var jwt = require('jsonwebtoken');

app.use('/carUsage', require('./routes/carUsage'));

app.get('/login', function(req,res){
  const payload = {
    id: 1,
    username: 'jay',
    email: 'jay@gslab.com' 
  }
  jwt.sign({payload}, 'key', { expiresIn: '30s'}, (err, token) =>  {
    res.json({token});
  });
});

db
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

app.listen(3005, ()=> console.log('express server running at port : 3005'));