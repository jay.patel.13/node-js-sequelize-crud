
const Sequelize = require('sequelize');
const db = require('../config/database');

const CarUsage = db.define('car_usage', {
  city: {
    type: Sequelize.STRING
  },
  car_id: {
    type: Sequelize.STRING
  },
  distance_covered: {
    type: Sequelize.DOUBLE
  },
  trip_start_timestamp: {
    type: Sequelize.BIGINT
  },
  trip_end_timestamp: {
    type: Sequelize.BIGINT
  }
},{
  freezeTableName:true,
  timestamps: false
});

module.exports = CarUsage;